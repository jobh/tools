import sys, os, urllib, gzip, hashlib, shutil

ext = '.hlink'
urlbase='http://simula.no/~jobh/assets'

def warn(msg):
    print '!!! '+msg

def usage():
    print 'Usage: %s <command>'
    print 'Commands:'
    print '    status       Print status'
    print '    pull         Download assets if necessary, and update files'
    print '    add <paths>  Create the .hlink file and associated downloadable asset'

class DataURLOpener(urllib.FancyURLopener):
    def __init__(self, url, filename):
        urllib.FancyURLopener.__init__(self)
        self.url = url
        self.filename = filename
    def retrieve(self, reporter=None, data=None):
        urllib.FancyURLopener.retrieve(self, self.url, self.filename, reporter, data)
    def http_error_default(self, url, fp, errcode, errmsg, headers):
        raise IOError(str(errcode)+" "+errmsg+", "+self.url)

def hashfile(fname, blocksize=65536):
    hasher = hashlib.sha1()
    hasher.update('blob %u\0' % os.path.getsize(fname))
    with open(fname, 'rb') as afile:
        buf = afile.read(blocksize)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(blocksize)
    return hasher.hexdigest()

def retrieve(filename, target):
    url = urlbase+'/'+filename

    targetdir = os.path.abspath(os.path.dirname(target))
    if not os.path.isdir(targetdir):
        os.makedirs(targetdir)
    try:
        DataURLOpener(url, filename).retrieve()
    except:
        if os.path.exists(filename):
            os.remove(filename)
        raise

    os.rename(filename, target)

def simplify(fname):
    prefix = repo_root()
    if fname.startswith(prefix):
        fname = fname[len(prefix):]
    return fname.lstrip('/\\')

def update(hlinkname):
    with open(hlinkname) as f:
        digest = f.read()

    target = hlinkname[:-len(ext)]
    if os.path.isfile(target):
        cur_digest = hashfile(target)
        if cur_digest == digest:
            return

    if os.path.exists(target):
        warn('%s has local changes -- ignoring'%simplify(target))
        return

    fname = os.path.join(repo_root(), '.git', 'assets', digest)
    if not os.path.isfile(fname):
        urlname = digest
        try:
            print '%s not in cache, fetching from %s'%(simplify(target),urlbase)
            retrieve(urlname, fname)
            if hashfile(fname) != digest:
                warn('Retrieved file has wrong checksum --- removing')
                os.remove(target)
                return
        except IOError as e:
            warn(e.message)
            return

    print '%s: updated'%simplify(target)
    shutil.copy(fname, target)
    return

def get_all_hlinks(root):
    hlinks = []
    for folder, subs, files in os.walk(root):
        for filename in files:
            if filename.endswith(ext):
                hlinks.append(os.path.join(folder, filename))
    return hlinks

def updatetree(root):
    for fname in get_all_hlinks(root):
        update(fname)

def status(root):
    for hlinkname in get_all_hlinks(root):
        with open(hlinkname) as f:
            digest = f.read()

        digest_present = os.path.isfile(os.path.join(repo_root(), '.git', 'assets', digest))

        target = hlinkname[:-len(ext)]
        if os.path.isfile(target):
            cur_digest = hashfile(target)
            cur_digest_present = os.path.isfile(os.path.join(repo_root(), '.git', 'assets', cur_digest))
            if cur_digest == digest:
                if digest_present:
                    print '%-50s up-to-date'%simplify(target)
                else:
                    print '%-50s up-to-date, not in cache'%simplify(target)
                continue

        if os.path.exists(target):
            if digest_present and cur_digest_present:
                print '%-50s local changes, old and new in cache'%simplify(target)
            elif digest_present:
                print '%-50s local changes, old in cache'%simplify(target)
            elif cur_digest_present:
                print '%-50s local changes, only new in cache'%simplify(target)
            else:
                print '%-50s local changes, not in cache'%simplify(target)
            continue

        if digest_present:
            print '%-50s missing'%simplify(target)
        else:
            print '%-50s missing, not in cache'%simplify(target)

def repo_root():
    root = os.path.abspath(os.curdir)
    while not os.path.isdir(os.path.join(root, '.git')):
        new_root = os.path.abspath(os.path.join(root, '..'))
        if new_root == root:
            raise IOException('Not a git repository')
        root = new_root
    return root

def replacefile(fname):
    refdir = os.path.join(repo_root(), '.git', 'assets')
    if not os.path.isdir(refdir):
        warn('Creating directory %s'%simplify(refdir))
        os.makedirs(refdir)
    digest = hashfile(fname)

    with open(fname+ext+'.tmp', 'wb') as f:
        f.write(digest)
    os.rename(fname+ext+'.tmp', fname+ext)

    digestfname = os.path.join(refdir, digest)
    if not os.path.isfile(digestfname):
        shutil.copyfile(fname, digestfname)
        print 'Replaced %s with %s.'%(simplify(fname), simplify(fname)+ext)
        with open(os.path.join(refdir, 'asset.log'), 'ab') as log:
            log.write('%s ==> %s\n'%(fname, os.path.basename(digestfname)))
    else:
        warn('%s is already in cache'%simplify(digestfname))

if __name__ == '__main__':
    args = sys.argv[1:]
    if not args:
        usage()
        exit(1)

    cmd = args[0]
    args = args[1:]

    if cmd == 'add':
        for fname in args:
            replacefile(fname)
    elif cmd == 'pull':
        updatetree(repo_root())
    elif cmd == 'status':
        status(repo_root())
    else:
        warn('Unknown command: %s'%cmd)
        usage()
        exit(1)
